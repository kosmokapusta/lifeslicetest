# Lifeslice Test #

This project was made for android devices with version >= 21 (Lollipop).
Support of older versions can be done by request.

The main target of this project - impress the interviewer with out of box decisions and learn something new about android.

## libraries in use ##

* Data Binding to operate easily with UI components
* appcompat-v7:25.2.0 and design:25.2.0 for native android UI/UX
* recyclerview-v7:25.2.0 for using recycle views as lists, grids, etc.
* retrofit2, okhttp3 and okhttp3:logging-interceptor for communication with RESTful web services
* picasso:2.5.2 for easy presenting of images
* easyvideoplayer:0.3.0 as a test shot for new video player to do not spend much time on playback and UI

## raw description ##

MainActivity class is the root (host) activity for TagFragment and VideoFragment. Fragments are hosted in ViewPager with MainTabsPageAdapter that sets tabs and their titles. TagFragment and VideoFragment communicate through MainActivity using OnTagVideoFlowListener interface. Entered string from TagFragment passes to VideoFragment by findVideosByTag method to  request videos from API. Classes for RESTful services are in "rest" package. Models for parsing responses are in "models" package. After getting response, videos list applies to RecyclerView using VideosRecycleAdapter. Selection of active item going from top of the RecyclerView to the bottom. Selected video url passes from adapter to VideoFragment by OnVideoItemListener and auto plays in video player.

Have fun!