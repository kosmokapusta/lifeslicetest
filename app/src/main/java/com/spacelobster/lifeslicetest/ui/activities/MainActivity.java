package com.spacelobster.lifeslicetest.ui.activities;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.spacelobster.lifeslicetest.R;
import com.spacelobster.lifeslicetest.databinding.ActivityMainBinding;
import com.spacelobster.lifeslicetest.ui.adapters.MainTabsPageAdapter;
import com.spacelobster.lifeslicetest.ui.fragments.OnTagVideoFlowListener;


public class MainActivity extends AppCompatActivity implements OnTagVideoFlowListener {
    private ActivityMainBinding mBinding;
	private MainTabsPageAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_main);

        setSupportActionBar(mBinding.toolbar);
	    mPagerAdapter = new MainTabsPageAdapter(
	    		getSupportFragmentManager(),
			    getResources().getStringArray(R.array.main_tabs_titles));
        mBinding.container.setAdapter(mPagerAdapter);
        mBinding.tabs.setupWithViewPager(mBinding.container);
    }

	@Override
	public void onTagSelected(String tag) {
		mBinding.container.setCurrentItem(1, true);
		mPagerAdapter.getVideoFragment().findVideosByTag(tag);
		hideKeyboard();
	}

	private void hideKeyboard() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}
}
