package com.spacelobster.lifeslicetest.ui.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.spacelobster.lifeslicetest.databinding.ItemVideoBinding;
import com.spacelobster.lifeslicetest.models.Video;

import java.util.List;

public class VideosRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
	private List<Video> mVideos;
	private OnVideoItemListener mListener;
	private int mSelectedPosition = 0;
	private TextView mSelectedUsername;

	public VideosRecycleAdapter(List<Video> videos, OnVideoItemListener listener) {
		mVideos = videos;
		mListener = listener;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemVideoBinding binding = ItemVideoBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new ViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
		ItemVideoBinding binding = ((ViewHolder)holder).mBinding;
		binding.setVideo(mVideos.get(position));

		binding.username.setSelected(mSelectedPosition == position);
		if (mSelectedPosition == position) {
			mSelectedUsername = binding.username;

			if (mListener != null)
				mListener.onItemSelected(mVideos.get(position).getVideoUrl());
		}

		binding.getRoot().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectItem(holder);
			}
		});
	}

	@Override
	public int getItemCount() {
		return mVideos.size();
	}

	public void setVideoItems(List<Video> items) {
		mVideos.clear();
		mVideos.addAll(items);
		notifyDataSetChanged();
	}

	public void selectItem(RecyclerView.ViewHolder holder) {
		if (holder == null)
			return;

		mSelectedUsername.setSelected(false);
		((ViewHolder) holder).getBinding().username.setSelected(true);
		mSelectedPosition = holder.getAdapterPosition();
		mSelectedUsername = ((ViewHolder) holder).getBinding().username;

		if (mListener != null)
			mListener.onItemSelected(mVideos.get(mSelectedPosition).getVideoUrl());
	}

	public int getSelectedPosition() {
		return mSelectedPosition;
	}

	public void setSelectedPosition(int mSelectedPosition) {
		this.mSelectedPosition = mSelectedPosition;
	}

	private static class ViewHolder extends RecyclerView.ViewHolder {
		ItemVideoBinding mBinding;

		public ViewHolder(ItemVideoBinding binding) {
			super(binding.getRoot());
			mBinding = binding;
		}

		public ItemVideoBinding getBinding() {
			return mBinding;
		}
	}

	public interface OnVideoItemListener {
		void onItemSelected(String videoUrl );
	}
}
