package com.spacelobster.lifeslicetest.ui.fragments;

/**
 * A tag/video flow interface for communication between fragments and
 * {@link com.spacelobster.lifeslicetest.ui.activities.MainActivity}
 */
public interface OnTagVideoFlowListener {
	void onTagSelected(String tag);
}
