package com.spacelobster.lifeslicetest.ui.fragments;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.afollestad.easyvideoplayer.EasyVideoProgressCallback;
import com.spacelobster.lifeslicetest.databinding.FragmentVideoBinding;
import com.spacelobster.lifeslicetest.models.Video;
import com.spacelobster.lifeslicetest.models.VideoData;
import com.spacelobster.lifeslicetest.rest.RestBaseCallBack;
import com.spacelobster.lifeslicetest.rest.RestClient;
import com.spacelobster.lifeslicetest.ui.adapters.VideosRecycleAdapter;

import java.util.ArrayList;

/**
 * A {@link Fragment} subclass to retrive list of videos fetched
 * by tag from {@link TagFragment}.
 */
public class VideoFragment extends Fragment implements EasyVideoCallback, EasyVideoProgressCallback {

	private int PAGE = 0;
	private FragmentVideoBinding mBinding;
	private boolean mIsVideoStarted = false;

	private VideosRecycleAdapter mVideoListAdapter = new VideosRecycleAdapter(
			new ArrayList<Video>(),
			new VideosRecycleAdapter.OnVideoItemListener() {
				@Override
				public void onItemSelected(String videoUrl) {
					setVideoToPlayer(videoUrl);
				}
			}
	);

	public VideoFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		mBinding = FragmentVideoBinding.inflate(inflater);
		mBinding.videosList.setAdapter(mVideoListAdapter);
		mBinding.player.setAutoPlay(true);
		mBinding.player.setCallback(this);
		mBinding.player.setProgressCallback(this);
		mBinding.player.setHideControlsOnPlay(true);

		return mBinding.getRoot();
	}

	@Override
	public void onPause() {
		super.onPause();
		mBinding.player.pause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mBinding.player.release();
	}

	public void findVideosByTag(String tag) {
		if (mBinding.player.getVisibility() == View.GONE)
			mBinding.player.setVisibility(View.VISIBLE);

		// TODO implement infinite scroll
		RestClient.get().getVideosByTag(tag, PAGE).enqueue(new RestBaseCallBack<VideoData>() {
			@Override
			public void onSuccess(VideoData obj) {
				if (getActivity() != null && mBinding != null) {
					mVideoListAdapter.setVideoItems(obj.getData().getRecords());
				}
			}

			@Override
			public void onError(String error) {
				if (getActivity() != null)
					Toast.makeText(getActivity(), "Error occurred: " + error, Toast.LENGTH_LONG).show();
			}
		});
	}

	private void setVideoToPlayer(String url) {
		mIsVideoStarted = false;
		if (mBinding.player.isPlaying()) {
			mBinding.player.stop();
			mBinding.player.reset();
		}

		mBinding.player.setSource(Uri.parse(url));
	}

	@Override
	public void onStarted(EasyVideoPlayer player) {
		mIsVideoStarted = true;
		Log.d("VP", "onStarted");
	}

	@Override
	public void onPaused(EasyVideoPlayer player) {
	}

	@Override
	public void onPreparing(EasyVideoPlayer player) {
	}

	@Override
	public void onPrepared(EasyVideoPlayer player) {
		mBinding.player.start();
	}

	@Override
	public void onBuffering(int percent) {
	}

	@Override
	public void onError(EasyVideoPlayer player, Exception e) {
	}

	@Override
	public void onCompletion(EasyVideoPlayer player) {
		if (player.isPrepared() && mIsVideoStarted) {

			int nextVideoIndex = mVideoListAdapter.getSelectedPosition() + 1 <= mVideoListAdapter.getItemCount() - 1 ?
			mVideoListAdapter.getSelectedPosition() + 1 : 0;
			mVideoListAdapter.selectItem(mBinding.videosList.findViewHolderForAdapterPosition(nextVideoIndex));
		}
	}

	@Override
	public void onRetry(EasyVideoPlayer player, Uri source) {
	}

	@Override
	public void onSubmit(EasyVideoPlayer player, Uri source) {
	}

	@Override
	public void onVideoProgressUpdate(int position, int duration) {
		// TODO use this callback for fancy list item spinner
	}
}