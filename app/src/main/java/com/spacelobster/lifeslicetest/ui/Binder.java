package com.spacelobster.lifeslicetest.ui;


import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.spacelobster.lifeslicetest.R;
import com.squareup.picasso.Picasso;

public class Binder {

	@BindingAdapter(value = "remoteImage")
	public static void bindRemoteImage(ImageView view, String url) {
		Picasso.with(view.getContext())
				.load(url)
				.placeholder(R.drawable.ic_finn)
				.into(view);
	}
}
