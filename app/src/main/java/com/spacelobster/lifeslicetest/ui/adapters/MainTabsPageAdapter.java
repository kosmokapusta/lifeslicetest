package com.spacelobster.lifeslicetest.ui.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.spacelobster.lifeslicetest.ui.fragments.TagFragment;
import com.spacelobster.lifeslicetest.ui.fragments.VideoFragment;

/**
 * An adapter for ViePager container placed in
 * {@link com.spacelobster.lifeslicetest.ui.activities.MainActivity} root layout.
 */

public class MainTabsPageAdapter extends FragmentPagerAdapter {
	private TagFragment mTagFragment;
	private VideoFragment mVideoFragment;
	private String[] mTitles = new String[]{};

	public MainTabsPageAdapter(FragmentManager fm, String[] titles) {
		super(fm);

		mTagFragment = new TagFragment();
		mVideoFragment = new VideoFragment();
		mTitles = titles;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
			case 0:
				return mTagFragment;
			case 1:
				return mVideoFragment;
			default:
				return new Fragment();
		}
	}

	@Override
	public int getCount() {
		return mTitles.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mTitles[position];
	}

	public TagFragment getTagFragment() {
		return mTagFragment;
	}

	public void setTagFragment(TagFragment mTagFragment) {
		this.mTagFragment = mTagFragment;
	}

	public VideoFragment getVideoFragment() {
		return mVideoFragment;
	}

	public void setVideoFragment(VideoFragment mVideoFragment) {
		this.mVideoFragment = mVideoFragment;
	}
}
