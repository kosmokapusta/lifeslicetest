package com.spacelobster.lifeslicetest.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.spacelobster.lifeslicetest.databinding.FragmentTagBinding;

/**
 * A {@link Fragment} subclass to enter tag for video search implemented in {@link VideoFragment}.
 */
public class TagFragment extends Fragment {
	private FragmentTagBinding mBinding;
	private OnTagVideoFlowListener mListener;

	public TagFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {

		mBinding = FragmentTagBinding.inflate(inflater);

		mBinding.tagEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					setVideoTag();
					return true;
				}
				return false;
			}
		});

		mBinding.fabNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setVideoTag();
			}
		});

		return mBinding.getRoot();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnTagVideoFlowListener) {
			mListener = (OnTagVideoFlowListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnTagVideoFlowListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	private void setVideoTag() {
		mListener.onTagSelected(mBinding.tagEdit.getText().toString());
	}
}
