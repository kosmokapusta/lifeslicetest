package com.spacelobster.lifeslicetest.models;


public class Video {
	String username;
	String videoUrl;
	String avatarUrl;

	public Video(String username, String videoUrl, String avatarUrl) {
		this.username = username;
		this.videoUrl = videoUrl;
		this.avatarUrl = avatarUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
}
