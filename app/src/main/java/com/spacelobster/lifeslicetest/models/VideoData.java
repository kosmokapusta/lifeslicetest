package com.spacelobster.lifeslicetest.models;


import java.util.List;

public class VideoData {
	Data data;

	public VideoData(Data data) {
		this.data = data;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public class Data {
		List<Video> records;

		public Data(List<Video> records) {
			this.records = records;
		}

		public List<Video> getRecords() {
			return records;
		}

		public void setRecords(List<Video> records) {
			this.records = records;
		}
	}
}
