package com.spacelobster.lifeslicetest.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spacelobster.lifeslicetest.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static RestClientInterface mEndpoints;
    private static Retrofit mRetrofit;
	private static RestClient mInstance;

    static {
        setupRestClient();
    }

    private RestClient() { mInstance =  this; }

    public static RestClientInterface get() {
        return mEndpoints;
    }

	public static RestClient getClient() { return mInstance; }

    private static void setupRestClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder builder = chain.request().newBuilder();
                        builder.addHeader("Content-Type", "application/json").build();
                        return chain.proceed(builder.build());
                    }
                }).build();

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();

        mRetrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(defaultHttpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .build();


        mEndpoints  = mRetrofit.create(RestClientInterface.class);
    }

    public static Retrofit getRetrofit() {
	    return mRetrofit;
    }
}
