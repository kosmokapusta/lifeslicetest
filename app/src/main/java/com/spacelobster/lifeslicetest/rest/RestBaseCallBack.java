package com.spacelobster.lifeslicetest.rest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RestBaseCallBack<T>  implements Callback<T> {

	public RestBaseCallBack() {
	}

	@Override
	public void onResponse(Call call, Response response) {
		if (response.isSuccessful()) {
			onSuccess((T)response.body());
		} else {
			onError(response.errorBody().toString());
		}
	}

	@Override
	public void onFailure(Call call, Throwable t) {
		onError(t.getMessage());
	}

	public abstract void onSuccess(T obj);
	public abstract void onError(String error);
}
