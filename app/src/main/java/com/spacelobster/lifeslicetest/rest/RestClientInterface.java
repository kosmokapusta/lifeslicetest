package com.spacelobster.lifeslicetest.rest;

import com.spacelobster.lifeslicetest.models.VideoData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestClientInterface {
	@GET("timelines/tags/{tag}")
	Call<VideoData> getVideosByTag(@Path("tag") String tag, @Query("page") int pageNumber);
}
